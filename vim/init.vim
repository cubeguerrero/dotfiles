" Specify a directory for plugins
" Install Vim Plug if not installed
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
endif

call plug#begin('~/.config/nvim/plugged')

" General
Plug 'djoshea/vim-autoread'
Plug 'tpope/vim-eunuch'

" Tmux integration
Plug 'christoomey/vim-tmux-navigator'

" Programming
Plug 'sheerun/vim-polyglot'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'joukevandermaas/vim-ember-hbs'
Plug 'mattn/emmet-vim'
Plug 'rust-lang/rust.vim'

" Coding helpers
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-endwise'
Plug 'ntpeters/vim-better-whitespace'

" Git integration
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

" Editor UI
Plug 'arzg/vim-colors-xcode'
Plug 'Yggdroot/indentLine'

"" Denite
Plug 'Shougo/denite.nvim', { 'do' : ':UpdateRemotePlugins' }

" You will load your plugins here
" Make sure use single quotes
" Initialize plugin system
call plug#end()

filetype plugin indent on
syntax on

" General
" ================================================================================
let mapleader=" "
set cursorline
set splitbelow
set splitright
set fileencoding=utf-8
set showmatch
set noshowmode
set nostartofline
set hidden
set clipboard=unnamed

" True Color and Italic
" See https://medium.com/@dubistkomisch/how-to-actually-get-italics-and-true-colour-to-work-in-iterm-tmux-vim-9ebe55ebc2be
" ================================================================================
" let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
" let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
" set termguicolors

" Turn off swap files
" ================================================================================
set noswapfile
set nobackup
set nowb

" Persistent Undo
" ================================================================================
silent !mkdir ~/.vim/backups > /dev/null 2>&1
set undodir=~/.vim/backups
set undofile

" Indentation
" ================================================================================
set shiftwidth=2
set softtabstop=2
set expandtab
set autoindent
set smartindent
set number

" Python
" ================================================================================
let g:python_host_prog = "~/.asdf/shims/python"
let g:python3_host_prog = "~/.asdf/shims/python"

" Denite
" ================================================================================
call denite#custom#var('file/rec', 'command', ['ag', '--follow', '--nocolor', '--nogroup', '-g', ''])
call denite#custom#option('default', 'prompt', 'λ')
call denite#custom#var('grep', 'command', ['ag'])
call denite#custom#var('grep', 'default_opts', ['-i', '--vimgrep'])
call denite#custom#var('grep', 'recursive_opts', [])
call denite#custom#var('grep', 'pattern_opt', [])
call denite#custom#var('grep', 'separator', ['--'])
call denite#custom#var('grep', 'final_opts', [])
call denite#custom#source('file_rec', 'sorters', ['sorter_sublime'])
call denite#custom#filter('matcher_ignore_globs', 'ignore_globs',
      \ [ '.git/', '.ropeproject/', '__pycache__/*', '*.pyc', 'node_modules/',
      \   'venv/', 'images/', '*.min.*', 'img/', 'fonts/', '*.png'])

nmap <LEADER>p :Denite -start-filter file/rec<CR>
nmap <LEADER>b :Denite buffer<CR>
nnoremap \ :Denite grep<CR>

" Vimwiki
" ================================================================================
let personal_wiki = {
      \ 'path': '~/Notes',
      \ 'syntax': 'markdown',
      \ 'ext': '.md'
      \ }

let g:vimwiki_list = [personal_wiki]
let g:vimwiki_ext2syntax = {
      \ '.md': 'markdown',
      \ '.markdown': 'markdown',
      \ '.mdown': 'markdown'
      \ }

" Editor UI
" ================================================================================
" Statusline
set laststatus=2

" ColorScheme
colorscheme xcodelighthc

