if [[ -a $HOME/.asdf/asdf.sh ]]; then
    . $HOME/.asdf/asdf.sh
fi

if [[ -a $HOME/.cargo/env ]]; then
    source $HOME/.cargo/env
fi


export EDITOR="code"
export VISUAL="$EDITOR"
export LC_ALL="en_US.UTF-8"
export LANG="en_US.UTF-8"
export KERL_CONFIGURE_OPTIONS='--disable-debug --without-javac'
export FZF_DEFAULT_COMMAND='ag --hidden --ignore .git -g ""'
export RUBY_CONFIGURE_OPTS="--with-openssl-dir=$(brew --prefix openssl@1.1)"

alias v='nvim'
alias vim='nvim'
alias ssh='TERM=xterm-256color ssh'
alias dotfiles="cd ~/Code/dotfiles"
alias rm_psql_pid="rm /usr/local/var/postgres/postmaster.id"
alias be="bundle exec"
